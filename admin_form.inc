<?php

/**
 * Implements hook_form
 */
function worldmap_setting_form() {
  $form = array();

  $form['display_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of links'),
    '#default_value' => variable_get('onthisdate_maxdisp', 3),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t("The maximum number of links to display in the block."),
    '#required' => TRUE,
  );

  $form['voc_id'] = array(
    '#type' => 'radios',
    '#title' => t('List of current Vocabularies.'),
    '#position' => 'left',
    '#options' => get_all_vocabulary(),
    '#default_value' => variable_get('voc_id', array()),
    '#description' => t('List of vocabularies displayed as checkboxes'),
    '#prefix' => '<div id="dropdown_plans_replace2">',
    '#suffix' => '</div>',
  );

  $name = 'news';
  $myvoc = taxonomy_vocabulary_machine_name_load($name);
  $tree = taxonomy_get_tree($myvoc->vid);
  $term_ids = array();
  foreach ($tree as $term) {
    $term_ids[] = $term->tid;
  }


  $result = taxonomy_term_load_multiple($term_ids);
  $options = array();
  foreach ($result as $key => $tname) {
    $options[$key] = $tname->name;
  }

  $form['term_names'] = array(
    '#type' => 'radios',
    '#title' => 'Title',
    '#options' => $options,
    '#default_value' => variable_get('term_names', array())
  );

  $form['layer']['layer_image'] = array(
    '#title' => t('File'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('layer_image_fid', ''),
    '#upload_location' => 'public://change-background',
  );

  $form['contact_information'] = array(
    '#markup' => t('You can leave us a message using the contact form below.'),
  );

  $form['#submit'][] = 'worldmap_setting';

  return system_settings_form($form);
}

/**
 * page callback for admin form
 */
function worldmap_setting(&$form, &$form_state) {
  variable_set('onthisdate_maxdisp', $form_state['values']['display_size']);
  variable_set('voc_id', $form_state['values']['voc_id']);

//  $file = file_load($form_state['values']['layer_image']);
//  if ($file) {
//    $file->status = FILE_STATUS_PERMANENT;
//    file_save($file);
//    variable_set('layer_image_fid', $file->fid);
//  } else {
//    $file = file_load(variable_get('layer_image', ''));
//    if ($file->fid) {
//      file_delete($file, TRUE);
//    }
//  }

  
  $field_name = 'field_data_latitude';
  // Make sure the field doesn't already exist.
  if (!field_info_field($field_name)) {
    // Create the field.
    $field = array(
      'field_name' => $field_name,
      'type' => 'text',
      'settings' => array('max_length' => 255),
    );
    field_create_field($field);

    // Create the instance.
    $instance = array('field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => 'article',
      'label' => 'Latitude',
      'description' => 'Description latitude',
      'required' => FALSE,
    );
    field_create_instance($instance);

    watchdog('my_module', t('!field_name was added successfully.', array('!field_name' => $field_name)));
  } else {
    watchdog('my_module', t('!field_name already exists.', array('!field_name' => $field_name)));
  }
}

/**
 * get all vocabulary
 */
function get_all_vocabulary() {
  $vocabulary = taxonomy_get_vocabularies();
  $checklist_vocab_array = array(); /* Change to array('0' => '--none--'); if you want a none option */
  foreach ($vocabulary as $item) {
    $key = $item->vid;
    $value = $item->name;
    $checklist_vocab_array[$key] = $value;
  }
  return $checklist_vocab_array;
}

/**
 * function to get all the node ids directly mapped to the given taxonomy terms.
 * Returns Flat array of node ids belong to the given term ids.
 * @param
 *   $tid takes term id can be a single term or array of terms.
 * @return
 *   $nid/$nids returns node ids array or sinly node id directly mapped to the given taxonomy term.
 */
function get_nodes_directly_mapped_to_term($tid) {
  if (is_array($tid) && !empty($tid)) {
    foreach ($tid as $key => $value) {
      $nids[] = taxonomy_select_nodes($value);
      $flat_nids = multitosingle($nids);
    }
    return $flat_nids;
  } else {
    $nid = taxonomy_select_nodes($tid);
    return $nid;
  }
}
