<?php

/**
 * Implements hook_menu().
 */
function worldmap_menu() {
  $items = array();

  $items['admin/config/content/worldmap'] = array(
    'title' => 'Home Map',
    'description' => 'Configuration for Home map module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('worldmap_setting_form'),
    'access arguments' => array('access administration pages'),
    'file' => 'admin_form.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['world-map'] = array(
    'title' => '',
    'description' => 'World map',
    'page callback' => 'worldmap_block_content',
    //'access arguments' => array('access content'),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function worldmap_block_info() {
  $block = array();
  $block['world_map'] = array(
    'info' => t('World Map'),
    'cache' => DRUPAL_NO_CACHE
  );
  return $block;
}

function worldmap_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'world_map':
      $block['subject'] = '';
      $block['content'] = worldmap_block_content();
      break;
  }
  return $block;
}

/**
 * Callback function used in the block view to fetch the data and assign to
 * custom template to be shown for the map.
 * @return type
 */
function worldmap_block_content() {
  $home_map_width = 980;
  $home_map_height = 520;

  $variables = array(
    //'node_data' => get_node_data($home_map_width, $home_map_height),

    'map_data' => map_callback($home_map_width, $home_map_height),
  );
  $image_var = get_map_image();

  return theme('home_map_template', array('map_data' => $variables, 'image_var' => $image_var));
}

/**
 * Wrapper function to get image used in the template.
 *
 * @return string
 *  HTML for an image.
 */
function get_map_image() {
  $map_image = array(
    'path' => drupal_get_path('module', 'home_map') . '/images/map.jpg',
    'alt' => t('Map'),
  );

  return theme('image', $map_image);
}

/**
 * Implements hook_theme().
 *
 */
function worldmap_theme() {
  $theme = array(
    'home_map_template' => array(
      'variables' => array('data' => array()),
      'path' => drupal_get_path('module', 'home_map') . '/templates',
      'template' => 'world-map',
    ),
  );
  return $theme;
}

/**
 * Wrapper function to fetch the country related data used in Home page and Job page Map for Latitude and Longitude
 * @return array Country Array with details
 */
function map_callback($home_map_width, $home_map_height) {

  $query = db_select('node', 'n');

  $query->join('field_data_field_latitude', 'm', 'n.nid = m.entity_id'); //JOIN node with magazines
  $query->join('field_data_field_longitude', 'l', 'n.nid = l.entity_id');
  $query->join('field_data_field_category', 't', 'n.nid = t.entity_id');
  $query->condition('type', 'article')
      ->fields('n', array('title','nid'))
      ->fields('m',array('field_latitude_value'))
      ->fields('l',array('field_longitude_value'))
      ->fields('t',array('field_category_tid'));
  

  $result = $query->execute();
  //print_r($result);

  //$output = array();
  $offices_data = array();
  foreach ($result as $record) {
    dpm($record);
    $sub_arr = array();
    $office_tid = $record->nid;
    // $sub_arr['lat'] = $record->field_latitude_value;
    $sub_arr["title"] = t($record->title);
    $sub_arr["term"] = t($record->field_category_tid);
    $sub_arr["lat"] = $record->field_latitude_value;
    $sub_arr["lng"] = $record->field_longitude_value;
    $sub_arr["top"] = ylat_to_pixel($record->field_latitude_value, $home_map_height);
    $sub_arr["left"] = xlng_to_pixel($record->field_longitude_value, $home_map_width);
    $offices_data[$office_tid][] = $sub_arr;
  }
  return $offices_data;
}

/**
 * The below function calculate the max and min value based on the passed value.
 * https://developers.google.com/maps/documentation/javascript/examples/map-coordinates
 *
 * @param type $value
 * @param type $opt_min
 * @param type $opt_max
 * @return type
 */
function bound($value, $opt_min, $opt_max) {
  if ($opt_min != NULL)
    $value = max($value, $opt_min);
  if ($opt_max != NULL)
    $value = min($value, $opt_max);
  return $value;
}

/**
 * The function calculates and return the latitude value to pixels.
 * @param type $lat
 * @param type $height
 * @return type
 */
function ylat_to_pixel($lat, $height) {
  /*
    Formula
    (givenLat*heightOfContainerElement)/180
    where 360 is the total longitude in degrees
    Height is calculated from the bottom
   */
  $container_height = 256;
  $siny = bound(sin(deg2rad($lat)), -0.9999, 0.9999);
  $lat = 90 + 0.5 * log((1 + $siny) / (1 - $siny)) * (-$container_height / (2 * pi()));
  $lat = $lat * (980 / 256);
  return $lat;
}

/**
 * The function calculates and return the longitude value to pixels.
 * @param type $lat
 * @param type $height
 * @return type
 */
function xlng_to_pixel($lng, $width) {
  /*
    Formula
    (givenLng*widthOfContainerElement)/360
    where 360 is the total longitude in degrees
   */
  $container_width = 256;
  $lng = $lng + 180;
  $lng = $lng * $container_width / 360;
  $lng = $lng * (980 / 256);
  return $lng;
}
