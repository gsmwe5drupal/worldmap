<?php print variable_get('voc_id'); 
print variable_get('term_names');
?>
<div class="selector">
      <span class="selected"><?php print t('Stories from the field'); ?></span><?php
      $volcab = taxonomy_vocabulary_machine_name_load('news');
      $tree = taxonomy_get_tree($volcab->vid, 0, 1);
      $output = '<ul class="list topics">';
      $output .= '<li><a class="bg topics" href="topics">' . t('All Topics') . '</a></li>';
      foreach ($tree as $term) {
        $output .= '<li><a href="topic_' . $term->tid . '">' . t($term->name) . '</a></li>';
      }
      $output .= '</ul>';
      echo $output;
      ?>
    </div>


<div class="map-wrapper">
    <?php
    print $image_var;
    //print_r($map_data);
    ?>  
<?php
    foreach ($map_data as $pid => $st_arr) {
            foreach ($st_arr as $cid => $arr) {
              $st_title = $arr[0]['title'];
             // $st_short_title = $arr[0]['short_title'];
             // $st_image = $arr[0]['image'];
            //  $st_link = $arr[0]['node_link'];
              $st_term = $arr[0]['term'];
              $st_top = $arr[0]['top'] - 15;
              $st_left = $arr[0]['left'] - 31;
              $lat = $arr[0]['lat'];
              $lng = $arr[0]['lng'];
              //$news_type = $arr[0]['news_type'];
        ?>
      <div class="map_item" id="topic_<?php echo $st_term; ?>" data-ctop="<?php echo $lat; ?>" data-cleft="<?php echo $lng; ?>" style="top:<?php echo $st_top . 'px'; ?>; left: <?php echo $st_left . 'px'; ?>; display: block;">
          <a class="pin topics" title="<?php echo $st_title; ?>" href="#">V</a>

          <div class="info" style="display: none;">
              <hgroup>
                  <h2>News</h2>
                  <h3><a href="news/leaders-urge-access-reproductive-health-supplies-crisis-settings" title="Leaders urge access to reproductive health supplies in crisis settings">Leaders urge access to reproductive health supplies in crisis settings</a></h3>
                  <a href="news/leaders-urge-access-reproductive-health-supplies-crisis-settings" title="Leaders urge access to reproductive health supplies in crisis settings"><img src="http://www.unfpa.org/sites/default/files/styles/news_map/public/news/1Y1A9408_newsbanner.jpg?itok=skh0mZHe" alt="Leaders urge access to reproductive health supplies in crisis settings" title="Leaders urge access to reproductive health supplies in crisis settings"></a>
              </hgroup>
              <a class="close" href="#">Close</a>
              <span class="arrow"></span>
          </div>
      </div>
    <?php
            }
          
          }
            ?>
    
</div>
<style>
    div[id^="topic_"] {
        position: absolute;
    }
    .selector {
    position: absolute;
}

</style>